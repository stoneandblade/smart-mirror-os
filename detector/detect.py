#  MBTechWorks.com 2017
#  Use an HC-SR501 PIR to detect motion (infrared)

#!/usr/bin/python

import RPi.GPIO as GPIO
import time
import os

GPIO.setmode(GPIO.BOARD)            #Set GPIO to pin numbering
pir = 8                             #Assign pin 8 to PIR
button = 10                         #Assign pin 10 to button
GPIO.setup(pir, GPIO.IN)            #Setup GPIO pin PIR as input
GPIO.setup(10, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
print ("Sensor initializing . . .")
time.sleep(2)                       #Give sensor time to startup
print ("Active")
print ("Press Ctrl+c to end program")
os.system('xset dpms 10 120 0');

try:
  while True:
   if GPIO.input(pir) == True:      #If PIR pin goes high, motion is detected
      print ("Motion Detected");
      os.system('xset dpms force on');
      os.system('xset -dpms');
      os.system('xset +dpms');
   if GPIO.input(button) == GPIO.HIGH:
      print("Button was pushed!")
      os.system('sudo reboot');
   time.sleep(0.5)

except KeyboardInterrupt:           #Ctrl+c
  pass                              #Do nothing, continue to finally
    
finally:
  GPIO.cleanup()                    #reset all GPIO
  print ("Program ended")
    
