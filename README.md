# Smart Mirror OS

## Useful commands

- set script permissions `chmod +x {script-name}.sh`
- move file `sudo mv <source> <destination>`
- copy file `sudo cp <source> <destination>`
- create directory `sudo mkdir <path>`
- edit crontabs `crontab -e`
- reboot `sudo reboot`
- shutdown `sudo shutdown` or `sudo poweroff`
- get ip address `hostname -I`
- revert unstaged changes `git checkout -- <filename>`

## Creating OS from scratch

1. Write RaspberryPi OS to disk using the RaspberryPi Imager
2. Boot device, do initial setup and install updates
   - If production device, use strong password that is stored in private repo
3. If this is a development machine:
   - Turn on VNC (and SSH?) by clicking the Pi icon at the top left, then Preferences >>> Raspberry Pi Configuration >> Interfaces Tab
   - Setup git username and email
     - git config --global user.name "firstname lastname"
     - git config --global user.email "email address"
4. Run setup script
   - Download [os-setup-from-scratch.sh](https://bitbucket.org/stoneandblade/smart-mirror-os/src/master/scripts/os-setup-from-scratch.sh)
   - Grant the script permissions `sudo chmod +x os-setup-from-scratch.sh`
   - Run commands in blocks and review output
5. If this is a production machine, before writing image:
   - Remove git user settings
   - Remove wifi setup (stored settings and credentials)

## Wifi-setup commands

- Run manually `sudo systemctl start wifi-setup`
- View log `sudo journalctl -u wifi-setup`
