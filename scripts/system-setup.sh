#!/bin/bash
# This script sets up the mirror when run for the first time. It creates
# the system.json file with the system id and password.  It also creates
# the modules folder if it doesn't exist and clones the modules repo into it.

MIRRORHOME=/home/pi/mirror
SYSTEMPATH=$MIRRORHOME/system.json
MODULEDIRECTORY=$MIRRORHOME/MagicMirror/modules/smart-mirror


# create the modules folder and clone the modules repo into it
if [ ! -d "$MODULEDIRECTORY" ]; then
	echo Mirror modules directory not found, starting git clone
	mkdir -p $MODULEDIRECTORY
	cd $MODULEDIRECTORY
	git clone https://bitbucket.org/stoneandblade/smart-mirror-modules.git .
	echo Mirror modules directory created, git repo cloned
fi


MODULEVERSION=$(jq '.version' $MODULEDIRECTORY/package.json)
OSVERSION=$(jq '.version' $MIRRORHOME/package.json)

# make sure the system.json file exists
if [ ! -f "$SYSTEMPATH" ]; then
	# create variables
	TIMEZONE="America/New_York" # TIMEZONE="America/Denver"
	SYSTEMID=$(uuidgen)
	SYSTEMPASSWORD=$(uuidgen | sed -e "s/-//g" | cut -c1-14)
	
	# write system JSON file with UUID and default timezone
	jq -n --arg timezone $TIMEZONE --arg id $SYSTEMID --arg password $SYSTEMPASSWORD \
		--argjson modversion $MODULEVERSION  --argjson osversion $OSVERSION \
		'{"timezone": $timezone, "id": $id, "pw": $password, "os-version": $osversion, "module-version": $modversion}' > $SYSTEMPATH
	sudo chmod +x $SYSTEMPATH
	
	# TODO: make specific script for setting timezone
	sudo timedatectl set-timezone $TIMEZONE
else
	echo system.json file checked, id: $(jq .id $SYSTEMPATH)
fi



