#!/bin/bash
# This script pull the mirror's configuration file and checks for updates

MIRRORPATH=/home/pi/mirror
MIRRORCONFIG=$MIRRORPATH/MagicMirror/config/config.js
NEWCONFIG=$MIRRORPATH/temp/config.js.sample
CONFIGURL=https://smart-mirror-configs.s3-us-west-2.amazonaws.com/config.js

# download latest config and write to the temp directory
echo Downloading latest config file from S3
wget $CONFIGURL -O $NEWCONFIG

# check to see fi the file has changed
if ! cmp -s $MIRRORCONFIG $NEWCONFIG; then
	echo Config file has changed
	# copy the new config file to the smart mirror's config directory
	mv $NEWCONFIG $MIRRORCONFIG
	# restart the magic mirror
	bash $MAINPATH/scripts/start-mirror.sh
	echo Config file has been updated and mirror restarted
else
	echo Config file has no changes, no update necessary
fi

