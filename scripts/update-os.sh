#!/bin/bash
# This script updates the smart mirror OS from the git repo

MIRRORHOME=/home/pi/mirror
SYSTEMPATH=$MIRRORHOME/system.json
PACKAGEPATH=$MIRRORHOME/package.json
CURRENTVERSION=$(jq '."os-version"' $SYSTEMPATH)


# pull latest from the git repo
echo Mirror OS update started
cd $MIRRORHOME
git pull

# get the version just pulled and see if we have an update
UPDATEDVERSION=$(jq .version $PACKAGEPATH)
if [ $CURRENTVERSION != $UPDATEDVERSION ]; then
	# if update, set the new version in the system file and restart
	echo OS has been updated, about to restart
	echo "$(jq --argjson version $UPDATEDVERSION '."os-version"=$version' $SYSTEMPATH)" > $SYSTEMPATH
	sudo reboot
else
	echo Mirror OS update completed, version has not changed
fi
