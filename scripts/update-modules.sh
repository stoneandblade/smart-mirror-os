#!/bin/bash
# This script updates the smart mirror modules from the git repo

MIRRORHOME=/home/pi/mirror
SYSTEMPATH=$MIRRORHOME/system.json
MODULEPATH=$MIRRORHOME/MagicMirror/modules/smart-mirror
PACKAGEPATH=$MODULEPATH/package.json
CURRENTVERSION=$(jq '."module-version"' $SYSTEMPATH)


# pull latest from the git repo
echo Mirror modules update started
cd $MODULEPATH
git pull


# get the version just pulled and see if we have an update
UPDATEDVERSION=$(jq .version $PACKAGEPATH)
if [ $CURRENTVERSION != $UPDATEDVERSION ]; then
	# if update, set the new version in the system file and restart
	echo Mirror modules have been updated, about to restart
	echo "$(jq --argjson version $UPDATEDVERSION '."module-version"=$version' $SYSTEMPATH)" > $SYSTEMPATH
	sudo reboot
else
	echo Mirror modules update completed, version has not changed
fi
