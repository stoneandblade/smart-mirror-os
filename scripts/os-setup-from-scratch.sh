#!/bin/bash
# This script builds and configures the Smart Mirror OS

MIRRORHOME=/home/pi/mirror
SCRIPTSHOME=$MIRRORHOME/scripts
LOGHOME=$SCRIPTSHOME/logs
CONFIGHOME=$SCRIPTSHOME/config
WIFISETUP=$MIRRORHOME/wifi-setup
WIFISETUPCONFIG=$WIFISETUP/config

<< 'SECTION-ONE-COMMENT'

# create root folder
mkdir $MIRRORHOME
cd $MIRRORHOME

# install node
curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
sudo apt install -y nodejs

SECTION-ONE-COMMENT


<< 'SECTION-TWO-COMMENT'

# clone our version of the mirror os, install and setup MagicMirror
echo "# cloning mirror os repo ########################################"
git clone https://bitbucket.org/stoneandblade/smart-mirror-os.git .
echo "# installing MagicMirror ########################################"
cd MagicMirror
npm install
echo "# copying default mirror config #################################"
cp $CONFIGHOME/default-config.js $MIRRORHOME/MagicMirror/config/config.js

echo "# installing wifi-setup dependencies#############################"
cd $WIFISETUP
npm install

SECTION-TWO-COMMENT


<< 'SECTION-THREE-COMMENT'

# install dependencies
echo "# installing dependencies #######################################"
# JQ library for JSON parsing
sudo apt-get install jq
# UUID library for creating unique IDs
sudo apt install uuid-runtime

# add crontabs
echo "# adding crontab tasks ##########################################"
(crontab -l 2>/dev/null; echo "@reboot bash $SCRIPTSHOME/system-setup.sh >> $LOGHOME/log.log") | crontab -
(crontab -l 2>/dev/null; echo "@reboot bash $SCRIPTSHOME/start-mirror.sh >> $LOGHOME/log.log") | crontab -
(crontab -l 2>/dev/null; echo "0 */2 * * * bash $SCRIPTSHOME/update-modules.sh >> $LOGHOME/log.log") | crontab -
(crontab -l 2>/dev/null; echo "0 2 * * * bash $SCRIPTSHOME/update-os.sh >> $LOGHOME/log.log") | crontab -
# (crontab -l 2>/dev/null; echo "") | crontab -

SECTION-THREE-COMMENT


<< 'SECTION-FOUR-COMMENT'

# configure wifi-setup scripts
echo "# starting wifi setup scripts ###################################"
sudo apt-get install hostapd
sudo apt-get install dnsmasq
sudo systemctl unmask hostapd
sudo systemctl disable hostapd
sudo systemctl disable dnsmasq

SECTION-FOUR-COMMENT


<< 'SECTION-FIVE-COMMENT'

# Edit /etc/default/hostapd using sudo nano /etc/default/hostapd to add the line: DAEMON_CONF="/etc/hostapd/hostapd.conf"
sudo nano /etc/default/hostapd
sudo cp $WIFISETUPCONFIG/hostapd.conf /etc/hostapd/hostapd.conf
sudo cp $WIFISETUPCONFIG/dnsmasq.conf /etc/dnsmasq.conf
sudo cp $WIFISETUPCONFIG/config/wifi-setup.service /lib/systemd/system
sudo systemctl enable wifi-setup

# to manuall start wifi-setup use:
# sudo systemctl start wifi-setup
# sudo journalctl -u wifi-setup

SECTION-FIVE-COMMENT


